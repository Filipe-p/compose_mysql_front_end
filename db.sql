create database mysimpledb;

create table user(
    id int not null primary key,
    f_name varchar(30),
    l_name varchar(30),
    email varchar(30)
)